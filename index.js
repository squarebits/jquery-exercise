$(function(){
	$('.calc-output').val('noop');
	var buttons = $('#app').find('button');

	var currentOutputState = result;
	var firstParameter = ''+result;
	var secondParameter = '';
	var filingSecondParameter = false;
	var carryOver = true;
	var currentOperation = 'noop';
	var result = 0;

	buttons.click(function(event){
		
		var button = event.target;
		var passValue = button.innerText;

		if(isNaN(passValue)){
			if(passValue === '='){
			  var first = parseInt(firstParameter);
			  var second = parseInt(secondParameter);
	  
			  result = 0;
			  switch(currentOperation){
				case '+': result = first + second; break;
				case '-': result = first - second; break;
				case '*': result = first * second; break;
				case '/': result = first / second; break;
			  }
	  
			  currentOutputState = result;
			  firstParameter = ''+result;
			  secondParameter = '';
			  filingSecondParameter = false;
			  carryOver = true;
			  currentOperation = 'noop';
			} else{
			  filingSecondParameter = true;
			  currentOperation = passValue;
			}
		  } else{
			if(filingSecondParameter){
			  secondParameter = secondParameter + passValue;
			  currentOutputState = secondParameter;
			} else{
			  if(carryOver){
				firstParameter = '' + passValue;
				carryOver = false;
			  } else{
				firstParameter = firstParameter + passValue;
			  }
			  currentOutputState = firstParameter;
			}
		  }

		  $('.calc-output').val(currentOutputState);
	});
}
);